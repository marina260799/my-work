<div class="row">
    <div class="col-12 col-md-3" style="background-color:  #2E275B;">
        <nav class="navbar navbar-expand-md navbar-light bg-light" style="margin-top: 40px; padding: 0;">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavLeft" aria-controls="navbarNavLeft" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavLeft" style="background-color:  #2E275B; padding: 0;">
                <ul class="navbar-nav flex-column" style="width: 90%;">
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Prima Pagina <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Cuvant Inainte <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Indrumari <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Cuprins <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">CV <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Testimoniale<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="comanda.html" style="color: white">Comanda <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Tstoria in imagini <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Istoria in clasa XI-A <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link links" href="#" style="color: white">Subiecte rezolvate<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="col-12 col-md-9" style="background-color: gainsboro;">
        <div style="margin: 10px; background-color: white; padding: 10px;">
            <p>
                <b>Istorii, adică fapte şi evenimente povestite</b>, nu doar menţionate.<br>
                <b>Concepte explicate</b>, nu doar definite.<br>
                <b>Evenimente din istoria universală</b> necesare pentru înţelegerea istoriei românilor.<br>
                <b>Dicţionar de termeni istorici.</b><br><br>

                <b>Îndrumări pentru rezolvarea subiectelor de bacalaureat</b>, nu doar subiecte rezolvate.<br>
                <b>Criterii de evaluare pentru pregătirea examenului de bacalaureat.</b><br><br>
                <b>Toate adaptate stilului de învăţare al fiecăruia dintre voi, prin:</b><br>
                <img src="images/image2.png"><br>
            <p style="font-size: 17px"><b>Manualul conţine toate temele din programa de bacalaureat, dar şi temele de istoria României din programa pentru clasa a XI-a.</b>
                <span style="color: red;"><b>Temele de istorie universală pentru clasa a XI-a nu sunt cuprinse în manual, le găsiţi doar pe site!!!</b></span><p><br>
                <span style="color: red; font-size: 18px;"><b>Fiecare capitol al părţii I (vezi cuprinsul) are următoarea structură:</b></span>
            <ul style="font-size: 17px">
                <li><b>textul</b> lecţiilor (txete obligatorii şi texte facultative)</li>
                <li><b>ideile principale</b> ale capitolului</li>
                <li><b>exerciţii:</b> întrebări recapitulative şi eseuri</li>
                <li><b>surse istorice</b> propuse spre analiză după modelul subiectelor I şi II de bacalaureat</li>
            </ul><br>
            <span style="font-size: 20px; color: red;"><b>Pe site veţi găsi integral capitolele</b></span>
            <span style="font-size: 20px;"><b>Romanitatea românilor în viziunea istoricilor, Statul român modern de la proiect politic la realizarea României Mari, Constituţiile din România şi  părţi din celelalte capitole.</b></span><br><br>
            <span style="font-size: 20px; color: red;"><b>Doar pe site găsiţi şi 40 de hărţi indispensabile pentru învăţarea istoriei românilor, la secţiunea <i>Istoria în imagini</i> (manualul nu este ilustrat).</b></span>
            </p>
        </div>
    </div>
</div>