<html lang="en">

<?php include "parts/head.php" ?>

<body style="background-color: gray;">
<div class="container">

    <?php include "parts/header.php" ?>

    <div class="row">
        <div class="col-12 col-md-3" style="background-color:  #2E275B;">
            <nav class="navbar navbar-expand-md navbar-light bg-light" style="margin-top: 40px; padding: 0;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavLeft" aria-controls="navbarNavLeft" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavLeft" style="background-color:  #2E275B; padding: 0;">
                    <ul class="navbar-nav flex-column" style="width: 90%;">
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Prima Pagina <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Cuvant Inainte <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Indrumari <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Cuprins <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">CV <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Testimoniale<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="comanda.html" style="color: white">Comanda <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Tstoria in imagini <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Istoria in clasa XI-A <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Subiecte rezolvate<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-12 col-md-9" style="background-color: gainsboro;">
            <div style="margin: 30px 10px 10px 10px; background-color: white; padding: 10px;">
                <form>
                    <fieldset class="fieldset">
                        <legend class="legend">Contact</legend>
                        <div class="form-group row">
                            <label for="contact_form_nume" class="col-sm-2 col-form-label"><span style="font-size: 20px;"><b>Nume:</b></span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="contact_form_nume">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_form_mail" class="col-sm-2 col-form-label"><span style="font-size: 20px;"><b>Email</b></span></label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="contact_form_mail">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="contact_form_mesaj" class="col-sm-2 col-form-label"><span style="font-size: 20px;"><b>Mesaj</b></span></label>
                            <div class="col-sm-7">
                                <textarea class="form-control" id="contact_form_mesaj" rows="3"></textarea>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <div style="float: left">
                    <button id="form_submit" type="submit">Trimite</button>
                </div>
                <div style="margin-left: 13%">
                    <span style="font-size: small"><b>Cartea se expediaza prin <span style="color: red">Posta Româna</span> (vezi mai jos detalii)! Dupa ce ati trimis comanda, veti primi un <span style="color: red">email</span> (in maxim doua zile) cu solicitarea de a <span style="color: red">confirma comanda.</span></b></span>
                </div><br>
            </div>
        </div>
    </div>

    <?php include "parts/footer.php" ?>
</div>
</body>
</html>