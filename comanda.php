<html lang="en">
    <?php include "parts/head.php" ?>
<body style="background-color: gray;">
<div class="container">

    <?php include "parts/header.php" ?>

    <div class="row">
        <div class="col-12 col-md-3" style="background-color:  #2E275B;">
            <nav class="navbar navbar-expand-md navbar-light bg-light" style="margin-top: 40px; padding: 0;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavLeft" aria-controls="navbarNavLeft" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavLeft" style="background-color:  #2E275B; padding: 0;">
                    <ul class="navbar-nav flex-column" style="width: 90%;">
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Prima Pagina <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Cuvant Inainte <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Indrumari <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Cuprins <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">CV <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Testimoniale<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="comanda.html" style="color: white">Comanda <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Tstoria in imagini <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Istoria in clasa XI-A <i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link links" href="#" style="color: white">Subiecte rezolvate<i class="fa fa-book fa-fw" aria-hidden="true"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="col-12 col-md-9" style="background-color: gainsboro;">
            <div style="margin: 10px; background-color: white; padding: 10px;">
                <p style="font-size: 17px"><b>Un exemplar costa <span style="color: red">30 de lei</span>, plus <span style="color: red">taxele postale</span> (pretul exact se verifica folosind formularul de comanda de mai jos). La comanda de <span style="color: red">peste 15 exemplare</span>, taxele postale sunt platite de autoare.</b></p>
                <form>
                    <fieldset class="fieldset">
                        <legend class="legend">Comanda</legend>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 25%"><label for="comanda_form_numar_volume"><b>Numar de volume:</b></label></td>
                                <td style="width: 30%">
                                    <input type="text" name="comanda_form[numar_volume]" id="comanda_form_numar_volume"/>
                                </td>
                                <td> x 30 RON + 11 RON(taxe postale) = 41 RON</td>
                            </tr>
                            <tr>
                                <td style="width: 25%"><label for="comanda_form_nume"><b>Nume si prenume:</b></label></td>
                                <td><input type="text" name="comanda_form[nume]" id="comanda_form_nume"/></td>
                            </tr>
                            <tr>
                                <td style="width: 25%"><label for="comanda_form_mail"><b>E-mail:</b></label></td>
                                <td><input type="text" name="comanda_form[mail]" id="comanda_form_mail" /></td>
                            </tr>
                            <tr>
                                <td style="width: 25%"><label for="comanda_form_telefon"><b>Telefon:</b></label></td>
                                <td><input type="text" name="comanda_form[telefon]" id="comanda_form_telefon" /></td>
                            </tr>
                            <tr>
                                <td style="width: 25%"><label for="comanda_form_judet"><b>Judet:</b></label></td>
                                <td><input type="text" name="comanda_form[judet]" id="comanda_form_judet" /></td>
                            </tr>
                            <tr>
                                <td style="width: 25%"><label for="comanda_form_localitate"><b>Localitate:</b></label></td>
                                <td><input type="text" name="comanda_form[localitate]" id="comanda_form_localitate" /></td>
                            </tr>
                            <tr valign="top">
                                <td style="width: 25%;"><label for="comanda_form_adresa"><b>Adresa:</b></label></td>
                                <td><textarea class="form-control" id="comanda_form_adresa" style="width: 200px;"></textarea></td>
                            </tr>

                        </table>
                    </fieldset>
                </form>
                <div style="float: left">
                    <button id="form_submit" type="submit">Trimite</button>
                </div>
                <div style="margin-left: 13%">
                    <span style="font-size: small">Cartea se expediaza prin <span style="color: red">Posta Româna</span> (vezi mai jos detalii)! Dupa ce ati trimis comanda, veti primi un <span style="color: red">email</span> (in maxim doua zile) cu solicitarea de a <span style="color: red">confirma comanda.</span></span>
                </div><br><br>
                <h4>Manualul contine toate temele din programa de bacalaureat, dar si temele de istoria Romaniei din programa pentru clasa a XI-a.
                    <span style="color: red">Temele de istorie universala pentru clasa a XI-a nu sunt cuprinse in manual, le gasiti doar pe site!!!</span></h4><br>
                <h5>Informatii utile pentru expedierea prin posta:</h5><br>
                <ul>
                    <li style="font-size: smaller">
                        <b>Cartea este expediata de catre autoare din Cluj-Napoca si ajunge in orice oras din Romania in trei (maxim patru) zile lucratoare</b> de la data expedierii. In cazul in care sunteti din <span style="color: red">Cluj</span> sau aveti drum prin Cluj, puteti sa obtineti cartea direct de la autoare, fara a mai plati taxele postale. Pentru informatii sunati la 0720544801 (Rodica Toadere).
                    </li><br>
                    <li style="font-size: smaller">
                        Dupa ce ati comandat cartea, veti primi prin email o cerere de confirmare a comenzii. Daca nu confirmati comanda, cartea nu va fi expediata! Daca in doua zile lucratoare nu ati primit emailul respectiv, puteti solicita informatii suplimentare la nr. de telefon 0720544801. Este posibil ca emailul de la autoare sa intre in casuta spam, asa ca ar fi bine sa verificati si acolo. Este posibil si sa dati toate datele de contact gresite, astfel incat nu puteti fi contactat(a). In mod obisnuit cartea este expediata prima sau a doua zi dupa ce ati facut comanda.
                    </li><br>
                    <li style="font-size: smaller">
                        Cartea expediata prin Posta Româna se ridica de la oficiul postal de care apartineti, dupa ce primiti acasa un aviz postal.
                    </li><br>
                    <li style="font-size: smaller">
                        <span style="color: red"><b>Adresa de expeditie pentru Posta Româna pe care o scrieti trebuie sa fie aceeasi cu adresa din buletinul dumneavoastra(domiciliu sau flotant);</b></span> în caz contrar nu puteti ridica pachetul !!!!!!!! Daca nu locuiti la adresa din buletin si nu ajungeti în timp util acasa, puteti sa cereti unui prieten sa va dea adresa lui si sa va ridice cartea. O alta varianta ar fi sa dati adresa parintilor si sa le cereti lor sa o ridice.
                    </li><br>
                    <li style="font-size: smaller">
                        <span style="color: red"><b>În cazul în care nu ati ridicat coletul în zece zile lucratoare de la data primului aviz, coletul se întoarce la expeditor.</b></span> În acest caz expeditorul (autoarea manualului) este obligata sa plateasca taxele postale dus-întors în valoare de 17 lei pentru un exemplar. În cazul în care doriti totusi sa mai primiti cartea, <span style="color: red"><b>va trebui sa achitati si acesti 17 lei</b></span> alaturi de valoarea normala de 40 de lei a cartii expediate.
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <?php include "parts/footer.php" ?>
</div>

</body>
</html>